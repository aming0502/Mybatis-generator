package com.talent.platform.demo.book;

import com.talent.platform.demo.book.dao.TModelDetailMapper;
import com.talent.platform.demo.book.model.TModelDetail;
import com.talent.platform.demo.book.model.TModelDetailExample;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: lixiaoming@qmadou.com
 * Date: 13-5-13
 * Time: 下午1:45
 * To change this template use File | Settings | File Templates.
 */
public class TTT {
    public  static void main(String[] args){
        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("config/spring-ds.xml");
        TModelDetailMapper mapper = (TModelDetailMapper) applicationContext.getBean("tModelDetailMapper");
        TModelDetail tModelDetail = new TModelDetail();
        tModelDetail.setModelId("testID");
        tModelDetail.setWeiboId("weiboID");
        mapper.insert(tModelDetail);


        TModelDetailExample example = new TModelDetailExample();
        TModelDetailExample.Criteria criteria = example.createCriteria();
        criteria.andWeiboIdEqualTo("weiboID");
        List<TModelDetail> lst = mapper.selectByExample(example);
        System.out.println(lst.size());

    }
}
